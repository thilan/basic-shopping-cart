<?php

/**
 * Shopping Cart (without using inbuilt codeigniter cart functionality) 
 * by Thilan Pathirage
 * 
 * Framework : CodeIgniter
 * PHP Version : 7.0
 *
 * @author Thilan Pathirage
 */
class Cart extends CI_Controller {

    /**
      | -------------------------------------------------------------------------
      | Private Variables that needed
      | -------------------------------------------------------------------------
     */
    /* Basic Array Variable for Storing data.
     *      Default Value:
     *          Array
     * 
     */
    private $products = [];

    /*
     * Constructor initialisation
     */

    public function __construct() {
        parent::__construct();

        /*
         * Load the Cart Model
         */
        $this->load->model('CartModel');

        /*
         * Load the Unit Test Library in CI
         */
        $this->load->library("unit_test");

        /*
         * Add some example Products
         */
        $this->products = [
            ["pid" => 1, "name" => "Sledgehammer", "price" => 125.75],
            ["pid" => 2, "name" => "Axe", "price" => 190.50],
            ["pid" => 3, "name" => "Bandsaw", "price" => 562.131],
            ["pid" => 4, "name" => "Chisel", "price" => 12.9],
            ["pid" => 5, "name" => "Hacksaw", "price" => 18.45]
        ];
    }

    /**
     * Unit Testing Function
     * 
     * This is a simple function to do the unit testing in the controller
     * 
     * @param Mixed $test The tests we need to preform
     * @param Mixed $result expected result
     * @param String $name Test Name
     * 
     * 
     * @return String Unit Test Report. 
     */
    private function unit_testing($test, $result, $name) {
        echo $this->unit->run($test, $result, $name);
    }

    /**
     * Index Page
     * 
     * This is the Home index page function
     *  
     */
    public function index() {
        $data = array();
        $data["products"] = $this->products;
        $this->load->view('cartview', $data);
    }

    /**
     * Add Product 
     * 
     * This Function will add the requested products to the session cart
     *  
     */
    public function add_product() {
        $res = $this->input->get("pid");
        $key = array_search($res, array_column($this->products, 'pid'));
        if ($key . "" != "") {
            $this->CartModel->add_product_to_cart($this->products[$key]);
            //$this->unit_testing($this->CartModel->add_product_to_cart($this->products[$key]), array(), "Add to cart function");
            redirect(base_url());
        } else {
            echo 'no product';
        }
    }

    /**
     * Delete Products
     * 
     * This Function will delete the requested products to the session cart
     *  
     */
    public function delete_product() {
        $res = $this->input->get("pid");
        if (isset($_SESSION["cart"])) {
            $this->CartModel->delete_items_from_cart($res);
            //$this->unit_testing($this->CartModel->delete_items_from_cart($res), array(), "Delete Cart function");
            redirect(base_url());
        } else {
            echo 'Cart is Empty';
        }
    }

}
