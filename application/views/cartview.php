<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Shopping Cart by Thilan Pathirage</title>
    </head>
    <body>
        <h3>Basic Shopping cart by Thilan Pathirage</h3>
        <hr/>
        <h4>Products</h4>
        <table border="1" >
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $value) { ?>
                    <tr>
                        <td><?php echo $value["name"]; ?></td>
                        <td><?php echo $value["price"]; ?></td>
                        <td><a href="<?php echo base_url("Cart/add_product?pid=" . $value["pid"]); ?>" >Add to Cart</a></td>
                    </tr>  
                <?php }
                ?>
            </tbody>
        </table>

        <hr/>
        <h4>Your Cart</h4>

        <?php if (isset($_SESSION["cart"])) { ?>
            <table border="1" >
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $cart = $_SESSION["cart"];
                    foreach ($cart["products"] as $value2) {
                        ?>
                        <tr>
                            <td><?php echo $value2["name"]; ?></td>
                            <td><?php echo $value2["price"]; ?></td>
                            <td><?php echo $value2["qty"]; ?></td>
                            <td><?php echo ($value2["qty"] * $value2["price"]); ?></td>
                            <td><a href="<?php echo base_url("Cart/delete_product?pid=" . $value2["pid"]); ?>">Delete Item</a></td>
                        </tr>  
                    <?php }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            echo '<p>Cart is Empty</p>';
        }
        ?>

    </body>
</html>
