<?php

/**
 * Cart Model
 *
 * This model will not handle any DB queries. it will handle only the session 
 * storage functionalities. 
 *
 * @author Thilan Pathirage
 */
class CartModel extends CI_Model {

    /**
     * add_product_to_cart
     * 
     * This Function has the algorithms to add products to the session cart 
     * 
     * @param Array $parry Product array 
     * 
     */
    function add_product_to_cart($parry) {
        if (isset($_SESSION["cart"])) {
            $session = $this->session->userdata("cart");
            $products = $session["products"];
            if (in_array($parry["pid"], array_column($products, 'pid'))) {
                $key = array_search($parry["pid"], array_column($products, 'pid'));
                $products[$key]["qty"] = $products[$key]["qty"] + 1;
                $session["products"] = $products;
                $this->session->set_userdata("cart", $session);
            } else {
                $parry["qty"] = 1;
                array_push($products, $parry);
                $session["products"] = $products;
                $this->session->set_userdata("cart", $session);
            }
        } else {
            $parry["qty"] = 1;
            $cart = [];
            $cart["cart"]["products"] = [];
            array_push($cart["cart"]["products"], $parry);
            $cart["cart"]["total"] = $parry["price"];
            $this->session->set_userdata($cart);
        }
    }

    /**
     * delete_items_from_cart
     * 
     * This Function has the algorithms to delete products to the session cart 
     *  
     * @param int $pid Product ID
     * 
     */
    function delete_items_from_cart($pid) {
        $session = $this->session->userdata("cart");
        $products = $session["products"];
        if (count($products) == 1) {
            unset($_SESSION["cart"]);
        } else {
            $key = array_search($pid, array_column($products, 'pid'));
            unset($products[$key]);
            $session["products"] = $products;
            $this->session->set_userdata("cart", $session);
        }
    }

}
